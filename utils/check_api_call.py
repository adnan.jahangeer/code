#!/usr/bin/env python

from pexpect import pxssh
import pexpect
import getpass
import time
import sys
import os
import signal

file_directory = "/home/emumba/Downloads/"
file1 = "logs_publishboks_GDHM.txt"
file2 = "logs_publishbooks_FHM.txt"
os.system("cat " + file_directory + file1 + " | grep 'Publishing' > " + file_directory + file1 + "_filtered")
os.system("cat " + file_directory + file2 + " | grep 'Publishing' > " + file_directory + file2 + "_filtered")
os.system("cat " + file_directory + file1 + "_filtered | sed 's/\(.*Publishing\)/Publishing/' > " + file_directory + file1 + "removed_timestamp")
os.system("cat " + file_directory + file2 + "_filtered | sed 's/\(.*Publishing\)/Publishing/' > " + file_directory + file2 + "removed_timestamp")
os.system("diff "+ file_directory + file1 + "removed_timestamp " + file_directory + file2 + "removed_timestamp > " + file_directory + "logs_publishboks-diff.log")
os.system("[ -s " + file_directory + "logs_publishboks-diff.log" ' ] && echo "\e[91mPublishing_logs_matching_failed' + '" || echo "\e[92mPublishing_logs_matched' + '"' )
os.system("rm " + file_directory + file1 + "_filtered" + " " + file_directory + file2 + "_filtered")