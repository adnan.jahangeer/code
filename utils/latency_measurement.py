#!/usr/bin/env python

from pexpect import pxssh
import pexpect
import getpass
import time
import sys
import os
import signal

def signal_handler(sig, frame):
    observer.sendline("pkill -9 tcpdump")
    time.sleep(1)
    observer.sendline("ps -ef | grep 'tcpdump' | grep -v 'grep' | wc -l")
    still_running = observer.expect(['0', '1'])
    while still_running == 1:
        print ("observer: tcpdump application still running, trying to kill again..")
        observer.sendline("pkill -9 tcpdump")
        time.sleep(1)
        observer.sendline("ps -ef | grep 'tcpdump' | grep -v 'grep' | wc -l")
        still_running = observer.expect(['0', '1'])
    sys.exit(0)


username = "emumba"
password = "eMumba-RCaV8m8L"

hostname = "10.54.88.55"

try:
    #BTS1
    observer = pxssh.pxssh(timeout=50000, logfile = sys.stdout)
    observer.login(hostname, username, password)
    observer.sendline("sudo timeout 120 tcpdump -i enp1s0f1np1 -j adapter_unsynced --time-stamp-precision nano -w ~/1d.pcap &")
    observer.expect("[sudo]")
    observer.sendline(password)
    time.sleep(1)
    observer.sendline("sudo timeout 120 tcpdump -i enp1s0f0np0 -j adapter_unsynced --time-stamp-precision nano host 224.0.31.1 -w ~/1e.pcap &")
    time.sleep(120)
    command1 = 'scp ' + username + '@' + hostname + ':/home/' + username +'/1d.pcap /home/emumba/'
    print (command1)
    child1 = pexpect.spawn(command1, timeout=50000)
    i = child1.expect(["password:", pexpect.EOF])
    if i==0: # send password                
        child1.sendline(password)
        child1.expect(pexpect.EOF)
        child1.close()
    elif i==1:  
        print ("Got the key or connection timeout")
        pass
    time.sleep(2)
    command1 = 'scp ' + username + '@' + hostname + ':/home/' + username +'/1e.pcap /home/emumba/'
    print(command1)
    child1 = pexpect.spawn(command1, timeout=50000)
    i = child1.expect(["password:", pexpect.EOF])
    if i==0: # send password                
        child1.sendline(password)
        child1.expect(pexpect.EOF)
        child1.close()
    elif i==1:  
        print ("Got the key or connection timeout")
        pass

    print ("Copied both files to machine")

except pxssh.ExceptionPxssh as e:
    print("ERROR: pxssh failed on login.")
    print(e)
    
