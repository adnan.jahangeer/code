#include "emumba-cme-mdp3-handler/API.h"
#include <atomic>
#include <iostream>
#include <signal.h>
using namespace std;

std::atomic<int> print_books;

void signal_handler(int signal_number) {
  // LOG_DIAG("Signal Received: {}", signal_number);
  switch (signal_number) {
  case SIGUSR1: {
    print_books.store(1);
  } break;
  }
}

int main() {
  emumba::cme::mdp3::FeedHandler _fh;

  emumba::cme::mdp3::FeedHandler _fh_interface;
  emumba::cme::mdp3::FeedListner *fh_api = nullptr;
  emumba::cme::mdp3::LoggingConfig _logging_config;
  _logging_config.severity = "info";
  _logging_config.is_console_enabled = true;
  _logging_config.is_file_enabled = true;
  //   _fh_interface.set_logging_configuration("info", true, true);
  emumba::cme::mdp3::ProcessorConfig processor_config;
  processor_config.core = 2;
//   processor_config.udp_config.interface = "lo";
  processor_config.udp_config.listen_ip = "127.0.0.1";
  processor_config.processor_name = "Testing_FH_as_library";
  emumba::cme::mdp3::ChannelConfig channel_config;
  channel_config.channel_id = "310";
  channel_config.connection_feeds =
      std::list<std::string>{"310IA", "310NA", "310SA"};
  processor_config.channels.push_back(channel_config);
  std::list<emumba::cme::mdp3::ProcessorConfig> _p_list;
  _p_list.push_back(processor_config);
  _fh_interface.set_processor_configuration(_p_list, _logging_config, fh_api);
  _fh_interface.run_channels();
  std::cout << "CME TESTING APP" << std::endl;
}