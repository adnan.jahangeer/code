#include<iostream>
#include "spdlog/spdlog.h"


using namespace std;

int main()
{
    spdlog::set_level(spdlog::level::info);
    SPDLOG_DEBUG("Debug level logging");
    SPDLOG_INFO("Info level logging");
    SPDLOG_WARN("Warn level logging");
    SPDLOG_ERROR("Err level logging");
    SPDLOG_TRACE("Trace level logging");
    return 0;
}