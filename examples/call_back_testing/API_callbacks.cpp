#include "API.h"
#include <iostream>
#include <list>
#include <string>
#include <unordered_map>

using namespace std;

class API {
public:
  void *_ptr;
  void onMarketstate(void *ptr) {
    cout << "On Market State Change " << *((int32_t *)(ptr)) << endl;
  }
};

class SecurityData {
public:
  SecurityData(int32_t secID) : _secID(secID){};
  int32_t _secID;
  void update_book() { cout << "Update book of secID: " << _secID << endl; }
};

class ChannelData {
public:
  std::unordered_map<int32_t, SecurityData> _whitelist;
};

class Channel {
public:
  API *ap;
  Channel(ChannelData &data)
      : _data(&data){

        };
  void process_incremental(int32_t secid) {
    if (_data->_whitelist.find(secid) != _data->_whitelist.end()) {
      _data->_whitelist.at(secid).update_book();
      ap->onMarketstate((void *)(&secid));
    }
  }
  ChannelData *_data;
};

int main() {
  emumba::cme::mdp3::FeedHandler _fh_interface;
  emumba::cme::mdp3::FeedListner *fh_api = nullptr;
  _fh_interface.set_logging_configuration("info", true, true);
  emumba::cme::mdp3::ProcessorConfig processor_config;
  processor_config.core = 2;
  processor_config.udp_config.interface = "lo";
  processor_config.udp_config.listen_ip = "127.0.0.1";
  processor_config.processor_name = "Testing_FH_as_library";
  emumba::cme::mdp3::ChannelConfig channel_config;
  channel_config.channel_id  = "310";
  channel_config.connection_feeds = std::list<std::string>{"310IA", "310NA", "310SA"};
  processor_config.channels .push_back(channel_config);
  _fh_interface.set_processor_configuration(processor_config, fh_api);

  // ChannelData ch1_data;
  // ch1_data._whitelist.emplace(1, SecurityData(1));
  // ch1_data._whitelist.emplace(2, SecurityData(2));
  // ch1_data._whitelist.emplace(3, SecurityData(3));
  // Channel channel_1(ch1_data);

  // while (true) {
  //   channel_1.process_incremental(1);
  //   channel_1.process_incremental(2);
  //   channel_1.process_incremental(3);
  // }

  // return 0;
}