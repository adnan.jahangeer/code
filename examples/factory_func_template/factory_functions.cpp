#include <iostream>
#include <vector>
#include <memory>
using namespace std;

template <typename T>
class Channel
{
public:
    Channel()
    {
    }
    void process_snapshot();
    void process();
};

class MBO_Channel : public Channel<MBO_Channel>
{
public:
    MBO_Channel()
    {
    }
    void process_snapshot();
};

template<class T>
void Channel<T>::process()
{
    cout << "process function \n";
    static_cast<T*>(this)->process_snapshot();
}

template<class T>
void Channel<T>::process_snapshot()
{
    cout << "process_snapshot" << endl;
}

void MBO_Channel::process_snapshot()
{
    cout << "MBO process_snapshot" << endl;
}

int main()
{
    // vector<shared_ptr<Channel<>>> channels;
    // channels.push_back(make_shared<Channel<>>());
    // channels.push_back(make_shared<MBO_Channel>());

    // for (auto _channel : channels)
    // {
    //     _channel->process();
    // }
}