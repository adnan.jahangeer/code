#include "spdlog/spdlog.h"
#include <iostream>
#include <mutex>
#include <pthread.h>
#include <thread>
#include <vector>

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
// #include

int main(int argc, const char **argv) {
  constexpr unsigned num_threads = 3;
  // A mutex ensures orderly access to std::cout from multiple threads.
  std::mutex iomutex;
  std::vector<std::thread> threads(num_threads);
  for (unsigned i = 0; i < num_threads; ++i) {
    int x = i + 1;
    threads[i] = std::thread([&iomutex, x] {
      std::this_thread::sleep_for(std::chrono::milliseconds(20));
      while (1) {
        {
          // Use a lexical scope and lock_guard to safely lock the mutex only
          // for the duration of std::cout usage.
          std::lock_guard<std::mutex> iolock(iomutex);
          std::cout << "Thread #" << x << ": on CPU " << sched_getcpu()
                    << " thread_id " << std::this_thread::get_id() << "\n";
        }

        // Simulate important work done by the tread by sleeping for a bit...
        std::this_thread::sleep_for(std::chrono::milliseconds(900));
      }
    });

    // Create a cpu_set_t object representing a set of CPUs. Clear it and mark
    // only CPU i as set.
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(i, &cpuset);
    int rc = pthread_setaffinity_np(threads[i].native_handle(),
                                    sizeof(cpu_set_t), &cpuset);

    pthread_setname_np(pthread_self(), "Thread1");
    if (rc != 0) {
      std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
    }
  }
  spdlog::info("Sample Info output.");
  spdlog::warn("Sample Warn output.");
  spdlog::error("Sample Error output.");

  for (auto &t : threads) {
    t.join();
  }
  return 0;
}