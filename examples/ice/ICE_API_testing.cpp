#include "impact-feed-handler/API.h"
#include <iostream>
using namespace std;

int main() {
  std::cout << "Hello World" << std::endl;
  emumba::ice::impact::FeedHandler _interface;
  emumba::ice::LogConfig log_config;
  _interface.configure_logging(log_config);

  return 0;
}
