#include <arpa/inet.h>
#include <chrono>
#include <fcntl.h>
#include <iostream>
#include <netinet/in.h>
#include <string.h>
#include <thread>
#include <unistd.h>
#define bzero(p, size) (void)memset((p), 0, (size))
using namespace std;
using namespace std::chrono;
using namespace std::this_thread;
// bool init_tcp_socket()
// {
//     }
//     return (_tcp_socket > -1);  //&& (fcntl(_tcp_socket, F_SETFL, O_NONBLOCK)
//     > -1);  //-1 is error
// }
bool connect_to_server(std::string ip, uint16_t port, int _tcp_socket) {
  /*---Initialize server address/port struct---*/
  struct sockaddr_in _tcp_server_addr;
  bzero(&_tcp_server_addr, sizeof(_tcp_server_addr));
  _tcp_server_addr.sin_family = AF_INET;
  _tcp_server_addr.sin_port = htons(port);
  if (inet_pton(AF_INET, ip.c_str(), &_tcp_server_addr.sin_addr.s_addr) == 0) {
    perror("ERROR: connect_to_server() - inet_pton()");
    return false;
  }
  /*---Connect to server---*/
  if (connect(_tcp_socket, (struct sockaddr *)&_tcp_server_addr,
              sizeof(struct sockaddr)) != 0) {
    // if (errno != EINPROGRESS)
    // {
    perror("ERROR: connect_to_server() - connect()");
    cout << "Errno: " << int(errno) << endl;
    return false;
    // }
  }
  return true;
}
bool is_connected(int sock) {
  unsigned char buf;
  //   int err = recv(sock, &buf, 1500, 0);
  buf = '2';
  int err = send(sock, &buf, 1, MSG_PEEK);
  if (err == -1 && errno != EAGAIN)
    cout << "Number of bytes send: " << err << endl;
  else if (err != -1)
    cout << "Number of bytes send: " << err << endl;

  // cout << "buf:" + buf << endl;
  return (err == -1 && errno != EAGAIN) ? false : true;
}
int main() {
  int _tcp_socket = socket(PF_INET, SOCK_STREAM, 0);
  fcntl(_tcp_socket, F_SETFL, O_NONBLOCK);
  connect_to_server("192.168.3.117", 50000, _tcp_socket);
  while (true) {
    // cout << "BEFORE SENDING DATA" << endl;
    if (is_connected(_tcp_socket)) {

      //   cout << "connected" << endl;
    } else {
      perror("Error Number: ");
      cout << "Not connected" << endl;
    }
    // cout << "AFTER SENDING DATA" << endl;
    // sleep_for(microseconds(1));
    for (int i = 0; i < 100; i++) {
      int error = 0;
      socklen_t size = sizeof(error);
      if (getsockopt(_tcp_socket, SOL_SOCKET, SO_ERROR, (char *)&error,
                     &size) != 0)
        perror("ERROR: getsockopt:");

      if (error != 0) {
        cout << "error:" << error << endl;

        // struct sockaddr_in _tcp_server_addr;
        // bzero(&_tcp_server_addr, sizeof(_tcp_server_addr));
        // _tcp_server_addr.sin_family = AF_UNSPEC;
        // connect(_tcp_socket, (struct sockaddr *)&_tcp_server_addr,
        //         sizeof(_tcp_server_addr));
        // sleep_for(seconds(10));
        // connect_to_server("192.168.3.117", 50000, _tcp_socket);
      }
      sleep_for(microseconds(100));
    }
  }
}