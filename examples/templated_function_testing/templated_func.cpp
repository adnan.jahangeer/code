
#include <cstdint>
#include <iostream>
using namespace std;

#define ALPHA 1
struct FieldType
{

    template <class T, class = typename std::enable_if<std::is_signed<T>::value>>
    unsigned char *get_field_value()
    {
        return (unsigned char *)(this + 1);
    }

    int16_t field_id;
    int16_t field_length;
};


// template <int16_t T=-1>
// int16_t *hello()
// {
//     cout << "Hello" << endl;
// }
template <int16_t T, class = typename std::enable_if<T >= 0 && T <= 1>::type>
int32_t *hello()
{
    cout << "Hello 1" << endl;
    return (int32_t *)0;
}
template <int16_t T, class = typename std::enable_if<T >= 2 && T <= 4>::type>
int64_t *hello()
{
    cout << "Hello 2" << endl;
    return (int64_t *)0;
}

int main()
{
   FieldType x;
   x.field_id = 1;
   x.field_length = 2;
    auto y = hello<ALPHA>();
}